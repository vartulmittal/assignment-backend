const { requestPageHTML } = require("./requestPageUtil");

async function scrapVideoPage(videoLink) {
  const videoPage = `https://youtube.com${videoLink}`;

  const requestData = await requestPageHTML(videoPage);

  try {
    return parseHtml(requestData.data);
  } catch (e) {
    return {
      likes: -1,
      dislikes: -1,
    };
  }
}

function parseHtml(html_data) {
  let JSONContent = html_data.split("var ytInitialData = ")[1].split(";</script>")[0];
  let jsonParsedContent;
  try {
    jsonParsedContent = JSON.parse(JSONContent);
  } catch (e) {
    jsonParsedContent = {};
  }
  let topLevelButtonsArray =
    jsonParsedContent?.contents?.twoColumnWatchNextResults?.results?.results
      ?.contents[0]?.videoPrimaryInfoRenderer?.videoActions?.menuRenderer
      ?.topLevelButtons;

  let topLevelButtonsLikedLabel =
    topLevelButtonsArray[0]?.toggleButtonRenderer?.defaultText?.accessibility
      .accessibilityData?.label;
  let topLevelButtonsDislikedLabel =
    topLevelButtonsArray[1]?.toggleButtonRenderer?.defaultText?.accessibility
      .accessibilityData?.label;
  let likes = populateCount(topLevelButtonsLikedLabel);
  let dislikes = populateCount(topLevelButtonsDislikedLabel);
  return {
    likes,
    dislikes,
  };
}

function populateCount(viewText) {
  return viewText.split(" ")[0];
}

module.exports = {
  scrapVideoPage,
};
