let thumbnails = [
  {
    height: 1280,
    width: 720,
    url: (videoId) => {
      return `https://i.ytimg.com/vi/${videoId}/maxresdefault.jpg`;
    },
  },
  {
    height: 640,
    width: 360,
    url: (videoId) => {
      return `https://i.ytimg.com/vi/${videoId}/sddefault.jpg`;
    },
  },
  {
    height: 480,
    width: 360,
    url: (videoId) => {
      return `https://i.ytimg.com/vi/${videoId}/hqdefault.jpg`;
    },
  },
  {
    height: 320,
    width: 160,
    url: (videoId) => {
      return `https://i.ytimg.com/vi/${videoId}/mqdefault.jpg`;
    },
  },
  {
    height: 120,
    width: 90,
    url: (videoId) => {
      return `https://i.ytimg.com/vi/${videoId}/default.jpg`;
    },
  }
];

function popultePossibleThumbnails(videoId) {
  return thumbnails.map((thumbnail) => {
    return {
      height: thumbnail.height,
      width: thumbnail.width,
      url: thumbnail.url(videoId),
    };
  });
}

module.exports = {
  popultePossibleThumbnails,
};
