const axios = require("axios");

let requestPageHTML = async function (page) {
  try {
    return await axios.get(page);
  } catch (e) {
    return {
      error: true,
      message: e,
    };
  }
};

module.exports = { requestPageHTML };
