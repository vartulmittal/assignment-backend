const { requestPageHTML } = require("./requestPageUtil");
const { popultePossibleThumbnails } = require("./thumbnail");

async function scrapTrending() {
  const trendingPage = "https://youtube.com/feed/trending";

  const requestData = await requestPageHTML(trendingPage);
  return parseHtml(requestData.data);
}

function parseHtml(html_data) {
  let JSONContent = html_data
    .split("var ytInitialData = ")[1]
    .split(";</script>")[0];

  let jsonParsedContent;
  try {
    jsonParsedContent = JSON.parse(JSONContent);
  } catch (e) {
    jsonParsedContent = {};
  }
  parsedJSONContentArray =
    jsonParsedContent?.contents?.twoColumnBrowseResultsRenderer?.tabs[0]
      .tabRenderer?.content?.sectionListRenderer?.contents;
  let videos = [];
  const current_time = Date.now();
  parsedJSONContentArray.forEach((data) => {
    const videoList = buildVideoArray(
      data.itemSectionRenderer.contents[0].shelfRenderer.content,
      current_time
    );
    videos = [...videos, ...videoList];
  });
  return videos;
}
function buildVideoArray(videoList, currentTime) {
  if (videoList.horizontalListRenderer) {
    return buildHorizontalVideos(
      videoList.horizontalListRenderer.items,
      currentTime
    );
  } else if (videoList.expandedShelfContentsRenderer) {
    return buildVerticalVideos(
      videoList.expandedShelfContentsRenderer.items,
      currentTime
    );
  }
  return [];
}

function buildHorizontalVideos(videoList, currentTime) {
  const videoEntryList = [];
  videoList.forEach((videoRenderer) => {
    videoRenderer = videoRenderer.gridVideoRenderer;
    let videoObject = {
      videoId: -1,
      title: "",
      description: "",
      videoUrl: "",
      videoThumbnails: [],
      viewCount: -1,
      channelName: "",
      channelId: "",
      channelUrl: "",
    };

    videoObject.videoId = videoRenderer.videoId;
    videoObject.videoUrl =
      videoRenderer.navigationEndpoint.commandMetadata.webCommandMetadata.url;
    videoObject.title = videoRenderer.title.runs[0].text;

    if (videoRenderer.hasOwnProperty("descriptionSnippet")) {
      videoObject.description = videoRenderer.descriptionSnippet.runs[0].text;
    }
    videoObject.channelName = videoRenderer.shortBylineText.runs[0].text;
    videoObject.channelId =
      videoRenderer.shortBylineText.runs[0].navigationEndpoint.browseEndpoint.browseId;
    videoObject.channelUrl =
      videoRenderer.shortBylineText.runs[0].navigationEndpoint.commandMetadata.webCommandMetadata.url;
    videoObject.viewCount = populateViewCount(
      videoRenderer.viewCountText.simpleText
    );

    videoObject.videoThumbnails = popultePossibleThumbnails(
      videoObject.videoId
    );

    videoEntryList.push(videoObject);
  });
  return videoEntryList;
}

function buildVerticalVideos(videoList, currentTime) {
  const videoEntryList = [];
  videoList.forEach((videoRenderer) => {
    videoRenderer = videoRenderer.videoRenderer;
    let videoObject = {
      videoId: -1,
      title: "",
      description: "",
      videoUrl: "",
      videoThumbnails: [],
      viewCount: -1,
      channelName: "",
      channelId: "",
      channelUrl: "",
    };
    videoObject.videoId = videoRenderer.videoId;
    videoObject.title = videoRenderer.title.runs[0].text;
    videoObject.videoUrl =
      videoRenderer.navigationEndpoint.commandMetadata.webCommandMetadata.url;
    videoObject.channelName = videoRenderer.longBylineText.runs[0].text;
    videoObject.channelId =
      videoRenderer.ownerText.runs[0].navigationEndpoint.browseEndpoint.browseId;
    videoObject.channelUrl =
      videoRenderer.longBylineText.runs[0].navigationEndpoint.commandMetadata.webCommandMetadata.url;
    videoObject.viewCount = populateViewCount(
      videoRenderer.viewCountText.simpleText
    );
    videoObject.videoThumbnails = popultePossibleThumbnails(
      videoObject.videoId
    );
    if (videoRenderer.hasOwnProperty("descriptionSnippet")) {
      videoObject.description = videoRenderer.descriptionSnippet.runs[0].text;
    }
    videoEntryList.push(videoObject);
  });
  return videoEntryList;
}

function populateViewCount(viewText) {
  let viewCount = 0;
  const viewersSplit = viewText.match(/(\d(\d)*)/g);
  for (let i = 0; i < viewersSplit.length; i++) {
    viewCount = viewCount * 1000 + Number(viewersSplit[i]);
  }
  return viewCount;
}

module.exports = {
  scrapTrending,
};
