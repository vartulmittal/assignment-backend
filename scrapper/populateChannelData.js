const { requestPageHTML } = require("./requestPageUtil");

async function scrapChannelPage(channelLink) {
  const videoPage = `https://youtube.com${channelLink}`;

  const requestData = await requestPageHTML(videoPage);

  try {
    return parseHtml(requestData.data);
  } catch (e) {
    return {
      subscribers: -1,
      thumbnails: [],
      description: "",
    };
  }
}

function parseHtml(html_data) {
  let JSONContent = html_data
    .split("var ytInitialData = ")[1]
    .split(";</script>")[0];

  let jsonParsedContent;
  try {
    jsonParsedContent = JSON.parse(JSONContent);
  } catch (e) {
    jsonParsedContent = {};
  }
  let headerData = jsonParsedContent?.header?.c4TabbedHeaderRenderer;
  let metaData = jsonParsedContent?.metadata?.channelMetadataRenderer;
  let subscribers = populateCount(headerData?.subscriberCountText?.simpleText);
  let desktopThumbnails = headerData?.banner?.thumbnails;
  let mobileThumbnails = headerData?.mobileBanner?.thumbnails;

  let description = metaData?.description;

  return {
    subscribers,
    desktopThumbnails,
    mobileThumbnails,
    description,
  };
}
function populateCount(viewText) {
  return viewText?.split(" ")[0];
}

module.exports = {
  scrapChannelPage,
};
