const path = require("path");
const AutoLoad = require("fastify-autoload");
const { connectMongo } = require("./db");

const mongoURI = process.env.MONGO_DB_URI;

module.exports = async function (fastify, opts) {
  connectMongo(mongoURI);
  fastify.register(require("fastify-cors"));

  fastify.register(AutoLoad, {
    dir: path.join(__dirname, "api", "routes"),
    options: Object.assign({}, opts),
  });
};
