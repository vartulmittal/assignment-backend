const mongoose = require("mongoose");
const { Schema } = mongoose;

const videoSchema = new Schema({
  videoId: { type: String, required: true },
  title: String,
  description: String,
  videoUrl: String,
  videoThumbnails: [Object],
  viewCount: Number,
  channelName: String,
  channelId: String,
  channelUrl: String,
});

const VideosSchema = mongoose.model("Videos", videoSchema, "videos");
module.exports = VideosSchema;
