const VideoSchema = require("../models/video.model");
const { scrapTrending } = require("../../scrapper/scrapTrendingVideos");
const { scrapChannelPage } = require("../../scrapper/populateChannelData");
const { scrapVideoPage } = require("../../scrapper/populateVideoPage");

let getVideos = async function () {
  try {
    const videos = await VideoSchema.find({});
    return videos;
  } catch (e) {
    throw Error(e);
  }
};

let populateVideos = async function () {
  let videos = await scrapTrending();

  videos.map(async (video) => {
    await VideoSchema.updateOne(
      {
        videoId: video.videoId,
      },
      video,
      {
        upsert: true,
      }
    );
  });
  try {
    return {};
  } catch (e) {
    throw Error(e);
  }
};

let getVideoDetailsById = async function (videoId) {
  try {
    let data = VideoSchema.findOne({ videoId });
    return data;
  } catch (e) {
    throw Error(e);
  }
};

let getVideoById = async function (videoId) {
  let videoData = await getVideoDetailsById(videoId);

  try {
    let [videoLikesDislikes, channelData] = await Promise.all([
      scrapVideoPage(videoData.videoUrl),
      scrapChannelPage(videoData.channelUrl),
    ]);
    return {
      videoLikesDislikes,
      channelData,
      videoData,
    };
  } catch (e) {
    throw Error(e);
  }
};

module.exports = {
  getVideos,
  populateVideos,
  getVideoById,
};
