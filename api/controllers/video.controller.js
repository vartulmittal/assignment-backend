const VideoService = require("../services/video.service");

let getVideos = async (req, res, next) => {
  try {
    const videos = await VideoService.getVideos();
    return res.status(200).send({
      status: 200,
      data: videos,
      message: "Successfully fetched videos",
    });
  } catch (e) {
    console.log(e);
    return res.status(400).send({ status: 400, message: e });
  }
};

let populateVideos = async (req, res, next) => {
  try {
    const videos = await VideoService.populateVideos();
    return res.status(202).send({
      status: 202,
      data: videos,
      message: "Populating videos",
    });
  } catch (e) {
    console.log(e);
    return res.status(400).send({ status: 400, message: e });
  }
};

let getVideoById = async (req, res, next) => {
  let videoId = req.params.id;
  try {
    const video = await VideoService.getVideoById(videoId);
    return res.status(200).send({
      status: 200,
      data: video,
      message: "Successfully fetched video",
    });
  } catch (e) {
    return res.status(400).send({ status: 400, message: e });
  }
};

module.exports = {
  getVideos,
  populateVideos,
  getVideoById
};
