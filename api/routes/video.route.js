const videoController = require("../controllers/video.controller");

module.exports = async function (fastify) {
  fastify.get("/videos", videoController.getVideos);
  fastify.post("/populate-videos", videoController.populateVideos);
  fastify.get("/video/:id", videoController.getVideoById);
};
